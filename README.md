# media

Images du site <lyceum.fr> pour l'éducation en lycée en licence open-source.

Actuellemnt hébérgé sur <https://lyceum.frama.io/media/>
<

## Création des fichiers

Les fichiers sont des fichiers `md` incluant des métadonnées dans leur en-tête.

Required keys:

- `type`: which type of media and builder should be used to build the medias.
- `licence`: a creative common licence if BY licence is used an `author` key must be provided.

## Media types

Each src media is a `md` file that must contain a `type` metadata tag.

Based on the `type` a specific build script is run: `utils/<type>/build.sh`.

### `tikz`

Standalone tikz image that produce `pdf`, `svg` and `png`s.

## Develop a new media

1. Run `npm start`.
2. Create a new `md` file
3. View changes on the browser.

## TODO

- [] create static website with a page for each media based on metadata.
- [] build medias and site with a docker script with CI.
- [] create a web interface(investigate https://tinacms.org/)
