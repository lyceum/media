const fs = require("fs");
const fsPromise = require("fs").promises;

const frontMatter = require("gray-matter");

module.exports = async function () {
  let images = [];
  const ids = await fsPromise.readdir("src/");

  ids.forEach((id) => {
    const metadata = frontMatter.read(`src/${id}`);
    images.push({ ...{ id: id.replace(/\.md$/, "") }, ...metadata });
  });

  return images;
};
