file=${1}
echo "-------------------------"
echo "Processing ${file}"
echo "-------------------------"

type=$(node utils/getType.js ${file})
echo Running processor $type

bash utils/${type}/build.sh ${file}
