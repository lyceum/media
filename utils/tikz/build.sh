BUILDER="tikz"
echo "-------------------------"
echo "Running builder $BUILDER on ${1}"

# file
SRC_FILENAME=`basename $1`
FILENAME="${SRC_FILENAME%.md}"

# dirs
SRC_DIR=`dirname $1`
ROOT_DIR=$(dirname $SRC_DIR)
BUILDER_DIR=$ROOT_DIR/utils/$BUILDER
OUT_DIR=$ROOT_DIR/public 
mkdir -p $OUT_DIR

# run pandoc to generate pdf on out_dir
pandoc $1 \
    --from markdown+raw_tex \
    --template=${BUILDER_DIR}/pandoc-template.tex \
    -o ${OUT_DIR}/${FILENAME}.tex

cd ${OUT_DIR}
# generate pdf    
#lualatex --interaction=nonstopmode ${FILENAME}.tex
# Convert to svg and png for easier usage
#pdf2svg "${FILENAME}.pdf" "${FILENAME}.svg"

# generate svg from dvi as mentionned in pgf doc
lualatex --output-format=dvi ${FILENAME}
dvisvgm ${FILENAME}
# TODO move this as a post process
for width in 256 512 1024 2048; do
    convert -background none -density $width -resize ${width}x "${FILENAME}.svg" "${FILENAME}-${width}.png"
done

# cleanup
rm *.{log,aux,tex,dvi,out.ps}
