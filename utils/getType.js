const frontMatter = require("gray-matter");

if (process.argv.length < 3) {
  console.error("Usage node getType.js file.md");
  process.exit(1);
}

const file = frontMatter.read(process.argv[2]);

console.log(file.data.type);
