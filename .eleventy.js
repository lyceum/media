module.exports = (eleventyConfig) => {
  // Copy our static assets to the output folder
  eleventyConfig.addPassthroughCopy({
    "./build/": "/",
    "./eleventy/favicons/": "/",
    "./eleventy/js/": "/js",
    "./eleventy/css/": "/css",
  });
  return {
    pathPrefix: "/media",
    dir: {
      input: "eleventy",
      output: "public",
    },
  };
};
